from django.db import models
class Catalog(models.Model):
    product = models.CharField(max_length=512)
    count = models.IntegerField()
    good = models.BooleanField(default=True)

    def __str__(self):
        return '{0.product}-{0.count}'.format(self)
# Create your models here.
